import * as React from 'react';
import { InputGroup, Button, Icon, IInputGroupProps } from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';

export interface IAutoAddingListItem {
  id?: string;
  index?: number;
  name: string;
  listName: string;
}
interface IState {
  deletingItems: IAutoAddingListItem[];
}
interface IProps {
  name: string;
  onAppendItem: (name: string) => void;
  onUpdateItem: (name: string, index: number, value: any) => void;
  onMarkForDeletion: (items: IAutoAddingListItem[], listName: string) => void;

  list: IAutoAddingListItem[];
}

class AutoAddingList extends React.Component<IProps, IState> {
  public static defaultProps = {
    list: []
  };
  constructor(props: IProps) {
    super(props);
    this.state = {
      deletingItems: []
    };
  }
  public appendItem = (event: React.MouseEvent) => {
    this.props.onAppendItem(this.props.name);
  };
  public changeItem = (
    item: IAutoAddingListItem,
    index: number,
    value: string
  ) => {
    this.props.onUpdateItem(this.props.name, index, { ...item, name: value });
  };
  public markForDeletion = (item: IAutoAddingListItem) => {
    const newState = this.isDeleting(item)
      ? this.state.deletingItems.filter(it => it.id !== item.id)
      : this.state.deletingItems.concat([
          { ...item, listName: this.props.name }
        ]);
    this.setState({ deletingItems: newState });
    this.props.onMarkForDeletion(newState, this.props.name);
  };
  public isDeleting = (item: IAutoAddingListItem) => {
    if (item.id) {
      return this.state.deletingItems.findIndex(it => it.id === item.id) !== -1;
    }
    return (
      this.state.deletingItems.findIndex(it => it.index === item.index) !== -1
    );
  };
  public render() {
    const { list } = this.props;
    return (
      <div>
        <Button onClick={this.appendItem}>
          {' '}
          <Icon icon={IconNames.ADD} />
        </Button>
        {list.map((item, index) => (
          <div style={{ display: 'flex' }} key={item.id || index}>
            <Input
              changeItem={this.changeItem}
              style={{
                backgroundColor: this.isDeleting({ ...item, index })
                  ? 'pink'
                  : 'white'
              }}
              item={item}
              index={index}
              defaultValue={item.name}
            />
            <Icon
              onClick={this.markForDeletion.bind(null, { ...item, index })}
              icon={IconNames.DELETE}
              color="red"
            />
          </div>
        ))}
      </div>
    );
  }
}

interface InputProps {
  index: number;
  item: IAutoAddingListItem;
  style?: React.CSSProperties;
  changeItem: (item: IAutoAddingListItem, index: number, value: any) => void;
}

const Input: React.SFC<InputProps & IInputGroupProps> = props => {
  const { changeItem, index, item, ...rest } = props;
  const onChange = (event: React.FormEvent<HTMLInputElement>) => {
    changeItem(item, index, event.currentTarget.value);
  };
  return <InputGroup onBlur={onChange} {...rest} />;
};

export default AutoAddingList;
