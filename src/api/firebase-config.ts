import * as firebase from 'firebase';
import { FirebaseOptions } from '@firebase/app-types';

export const firebaseOptions: FirebaseOptions = {
  apiKey: 'AIzaSyDZ0GJhkbQe1z_oulNPxlDa0Q5V8wz7DW4',
  authDomain: 'sheetsy-final.firebaseapp.com',
  databaseURL: 'https://sheetsy-final.firebaseio.com',
  messagingSenderId: '209074946533',
  projectId: 'sheetsy-final',
  storageBucket: ''
};

const firebaseApp: firebase.app.App = firebase.initializeApp(firebaseOptions);
export default firebaseApp;

export const COLLECTIONS = {
  PUBLIC: 'public',
  USERS: 'users',
  USERS_CHARACTERS: 'characters'
};
