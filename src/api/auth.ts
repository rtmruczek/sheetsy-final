import * as firebase from 'firebase';
import '@firebase/auth';
import { eventChannel, Channel } from 'redux-saga';
import { take, put, call } from 'redux-saga/effects';
import { authStateChanged } from '../reducers/user';

import firebaseApp from './firebase-config';

function firebaseAuthChannel() {
  return eventChannel(emit => {
    const onAuthStateChanged = (user: firebase.User) => {
      emit({ user });
    };
    firebase.auth(firebaseApp).onAuthStateChanged(onAuthStateChanged);

    // this has no unsubscribe function, so just return an empty function
    return () => { };
  });
}
export function* startListeningForAuth() {
  const channel: Channel<any> = yield call(firebaseAuthChannel);
  const payload: { user: firebase.User } = yield take(channel);
  yield put(authStateChanged(payload.user || {
    email: ''
  }));
}

const provider = new firebase.auth.GoogleAuthProvider();
export function loginWithGooglePopup() {
  firebase.auth(firebaseApp).signInWithPopup(provider);
}
export function loginWithGoogleRedirect() {
  firebase.auth(firebaseApp).signInWithRedirect(provider);
}
