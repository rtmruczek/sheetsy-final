import * as firebase from 'firebase';
import '@firebase/firestore';
import firebaseApp, { COLLECTIONS } from './firebase-config';
import { omit, keys } from 'ramda';
import * as uuid from 'uuid';

const db = firebase.firestore(firebaseApp);

const settings = { /* your settings... */ timestampsInSnapshots: true };
db.settings(settings);

export async function getAllDNDData(): Promise<
  firebase.firestore.DocumentData[]
> {
  try {
    const querySnapshot: firebase.firestore.QuerySnapshot = await db
      .collection(COLLECTIONS.PUBLIC)
      .get();
    return querySnapshot.docs.map(
      (doc: firebase.firestore.QueryDocumentSnapshot) => doc.data()
    );
  } catch (e) {
    throw e;
  }
}

export async function addOrUpdateDocumentsInCollection(
  collectionPath: string,
  values: any[]
) {
  const batch = db.batch();
  values.forEach(value => {
    if (value.id) {
      const dbRef = db.collection(collectionPath).doc(value.id);
      batch.set(dbRef, value, {
        mergeFields: keys(omit(['id'], value)) as string[]
      });
    } else {
      const id = uuid.v4();
      const dbRef = db.collection(collectionPath).doc(id);
      batch.set(dbRef, { ...value, id });
    }
  });
  return await batch.commit();
}

export async function updateDocumentsInCollection(
  collectionPath: string,
  values: any[]
) {
  const batch = db.batch();
  values.forEach(value => {
    const dbRef = db.collection(collectionPath).doc(value.id);
    batch.set(dbRef, value, {
      mergeFields: keys(omit(['id'], value)) as string[]
    });
  });
}

export async function deleteDocumentsInCollection(
  collectionPath: string,
  ids: string[]
) {
  const batch = db.batch();
  ids.forEach(id => {
    const dbRef = db.collection(collectionPath).doc(id);
    batch.delete(dbRef);
  });
  return await batch.commit();
}

export async function updateDocument(queryPath: string, value: any) {
  return await db.doc(queryPath).update(value);
}

export async function executeQuery(
  queryPath: string
): Promise<firebase.firestore.DocumentData[]> {
  const snapshot = await db
    .collection(queryPath)
    .orderBy('name', 'asc')
    .get();
  return snapshot.docs.map(doc => doc.data());
}
