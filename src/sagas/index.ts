import {
  all,
  call,
  fork,
  take,
  select,
  takeEvery,
  put
} from 'redux-saga/effects';
import { startListeningForAuth, loginWithGoogleRedirect } from '../api/auth';
import {
  executeQuery,
  updateDocument,
  addOrUpdateDocumentsInCollection,
  deleteDocumentsInCollection
} from '../api/firestore';
import { ICharacter, mapCharacters } from 'src/mappable/character';
import { find } from 'ramda';
import * as User from '../reducers/user';
import * as Games from '../reducers/games';
import { COLLECTIONS } from '../api/firebase-config';

export default function* mySaga() {
  yield fork(startListeningForAuth, 'authObserver');
  yield takeEvery(User.Actions.AUTH_STATE_CHANGED, loginIfNeeded);
  yield takeEvery(Games.Actions.REQUEST_PUBLIC_DATA, getPublicData);
  yield takeEvery(User.Actions.REQUEST_MY_DATA, getMyData);

  yield takeEvery(Games.Actions['ADMIN/REQUEST_SAVE_GAME_LISTS'], doSaveLists);
}

export function* loginIfNeeded(action: User.IAuthStateChangedAction) {
  if (!action.payload.email || action.payload.email.length === 0) {
    yield call(loginWithGoogleRedirect);
  } else {
    // logged in

    // wait for state to be updated with newest public data
    yield take(Games.Actions.RECEIVE_PUBLIC_DATA);

    // then request the user's data
    yield put({
      type: User.Actions.REQUEST_MY_DATA,
      payload: { id: action.payload.uid }
    });
  }
}

export function* doUpdateDocument(action: any) {
  yield call(updateDocument, action.path, action.value);
  // yield put({
  // type: Games.Actions.SUCCESS_UPDATE_DOCUMENT
  // });
}

export function* doSaveLists(action: any) {
  const state: { user: User.IUser; games: Games.IGames } = yield select();
  const baseCollectionPath = `games/${state.games.selectedGame}`;

  let deletedIds: string[] = [];
  const itemsToBeDeletedByCollection = state.games.markedForDeletion;
  const deleteFunctions = Object.keys(itemsToBeDeletedByCollection).map(key => {
    const collectionIds: string[] = itemsToBeDeletedByCollection[key]
      .filter(item => !!item.id)
      .map(item => item.id as string);
    deletedIds = deletedIds.concat(collectionIds);
    return call(
      deleteDocumentsInCollection,
      `${baseCollectionPath}/${key}`,
      collectionIds
    );
  });

  yield all(deleteFunctions);

  const filterOutDeletedIds = (list: any[]) => {
    return list.filter(item => deletedIds.indexOf(item.id) === -1);
  };
  yield all([
    call(
      addOrUpdateDocumentsInCollection,
      `${baseCollectionPath}/races`,
      filterOutDeletedIds(state.games.games[state.games.selectedGame].races)
    ),
    call(
      addOrUpdateDocumentsInCollection,
      `${baseCollectionPath}/skills`,
      filterOutDeletedIds(state.games.games[state.games.selectedGame].skills)
    ),
    call(
      addOrUpdateDocumentsInCollection,
      `${baseCollectionPath}/classes`,
      filterOutDeletedIds(state.games.games[state.games.selectedGame].classes)
    )
  ]);
  yield put({ type: Games.Actions.REQUEST_PUBLIC_DATA });
}

export function* getPublicData() {
  const games: firebase.firestore.DocumentData[] = yield call(
    executeQuery,
    `games`
  );

  const dnd = find(game => game.name === 'dnd', games);
  const [
    races,
    classes,
    skills
  ]: firebase.firestore.DocumentData[][] = yield all([
    call(executeQuery, `games/${dnd.id}/races`),
    call(executeQuery, `games/${dnd.id}/classes`),
    call(executeQuery, `games/${dnd.id}/skills`)
  ]);

  yield put({
    type: Games.Actions.RECEIVE_PUBLIC_DATA,
    payload: {
      [dnd.id]: {
        id: dnd.id,
        name: 'dnd',
        races,
        classes,
        skills
      }
    }
  });
}

function getMyCharactersQuery(action: any) {
  return call(
    executeQuery,
    `${COLLECTIONS.USERS}/${action.payload.id}/${COLLECTIONS.USERS_CHARACTERS}`
  );
}

export function* getMyData(action: any) {
  interface IPayload {
    characters: ICharacter[];
  }

  const state = yield select();

  const [characters]: firebase.firestore.DocumentData[][] = yield all([
    getMyCharactersQuery.call(null, action)
  ]);

  const payload: IPayload = {
    characters: mapCharacters(characters, state)
  };

  yield put({ type: User.Actions.RECEIVE_MY_DATA, payload });
}
