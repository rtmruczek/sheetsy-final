import * as React from 'react';
import { connect } from 'react-redux';
import { find } from 'ramda';
import { RouteComponentProps } from 'react-router';

import { ICharacter } from 'src/mappable/character';
import { IUser } from 'src/reducers/user';

interface IProps {
  user: IUser;
  character: ICharacter;
}
interface IState {
  loading: boolean;
}

class CharacterDetail extends React.Component<IProps, IState> {
  public static getDerivedStateFromProps(props: IProps) {
    return {
      loading: !props.character
    }
  }
  constructor(props: IProps) {
    super(props);
    this.state = {
      loading: true
    };
  }
  public render() {
    const { character } = this.props
    const { loading } = this.state
    if (loading) {
      return null
    }
    return (
      <div>
        <h1>Character Detail</h1>
        <h2>{character.name}</h2>
        <h2>Level {character.level} {character.class.name}</h2>
        <div style={{ display: 'flex' }}>
          <div>Attributes</div>
          <div>Skills</div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ user }: { user: IUser }, routeComponentProps: RouteComponentProps<{ id: string }>) => {
  return {
    user,
    character: find((character: ICharacter) => character.id === routeComponentProps.match.params.id, user.characters)
  }
}

export default connect(mapStateToProps)(CharacterDetail);