import * as React from 'react';
import { connect } from 'react-redux';
import { IUser } from '../reducers/user';
import { requestData } from '../reducers/games';
import { Dispatch } from 'redux';

interface Props extends IUser {
  requestPublicData: () => {};
}

class Home extends React.Component<Props> {
  constructor(props: Props) {
    super(props);
  }
  public render() {
    return <div>Hi, {this.props.email}</div>;
  }
}

const mapStateToProps = ({ user }: { user: IUser }) => ({
  email: user.email
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
  requestPublicData: () => dispatch(requestData())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
