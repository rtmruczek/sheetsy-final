import * as React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { ICharacter } from 'src/mappable/character';
import { IUser } from 'src/reducers/user';

export interface CharactersProps {
  characters: ICharacter[];
}

class Characters extends React.Component<CharactersProps, any> {
  public render() {
    return (<div>
      {
        this.props.characters.map(character => (
          <div key={character.id}>
            <Link to={`characters/${character.id}`}>{character.id}</Link>
          </div>
        ))
      }
    </div>)
  }
}

const mapState2Props = ({ user }: { user: IUser }) => {
  return {
    characters: user.characters
  };
};

export default connect(mapState2Props)(Characters);
