import * as React from 'react';
import { connect } from 'react-redux';
import { IGames, Actions } from '../reducers/games';
import { Spinner, Button } from '@blueprintjs/core';
import AutoAddingList, {
  IAutoAddingListItem
} from 'src/components/AutoAddingList';
import { Dispatch } from 'redux';

export interface IProps {
  races: any[];
  classes: any[];
  skills: any[];
  isFetching: boolean;
}

interface DispatchProps {
  updateListItem: (listName: string, index: number, value: string) => void;
  appendListItem: (listName: string) => void;
  updateDeletions: (items: IAutoAddingListItem[], listName: string) => void;
  saveLists: () => void;
}

const columnStyle: React.CSSProperties = {
  marginRight: 10
};

class Admin extends React.Component<IProps & DispatchProps> {
  public render() {
    const { races, classes, skills, isFetching } = this.props;
    if (isFetching) {
      return <Spinner />;
    }
    return (
      <div>
        <h1>admin</h1>
        <div style={{ display: 'flex' }}>
          <div style={columnStyle}>
            <h2>Races</h2>
            <AutoAddingList
              name="races"
              onAppendItem={this.appendListItem}
              onUpdateItem={this.updateListItem}
              onMarkForDeletion={this.updateDeletions}
              list={races}
            />
          </div>
          <div style={columnStyle}>
            <h2>Classes</h2>

            <AutoAddingList
              name="classes"
              onAppendItem={this.appendListItem}
              onUpdateItem={this.updateListItem}
              onMarkForDeletion={this.updateDeletions}
              list={classes}
            />
          </div>
          <div style={columnStyle}>
            <h2>Skills</h2>
            <AutoAddingList
              name="skills"
              onAppendItem={this.appendListItem}
              onUpdateItem={this.updateListItem}
              onMarkForDeletion={this.updateDeletions}
              list={skills}
            />
          </div>
        </div>
        <Button onClick={this.props.saveLists} intent="primary">
          Save
        </Button>
      </div>
    );
  }

  public updateListItem = (name: string, index: number, value: string) => {
    this.props.updateListItem(name, index, value);
  };
  public appendListItem = (name: string) => {
    this.props.appendListItem(name);
  };
  public updateDeletions = (items: IAutoAddingListItem[], listName: string) => {
    this.props.updateDeletions(items, listName);
  };
}

const mapStateToProps = ({ games }: { games: IGames }): IProps => {
  const game = games.games[games.selectedGame];
  return {
    isFetching: !game,
    races: game ? games.games[games.selectedGame].races : [],
    classes: game ? games.games[games.selectedGame].classes : [],
    skills: game ? games.games[games.selectedGame].skills : []
  };
};

const mapDispatchToProps = (dispatch: Dispatch) => ({
  saveLists: () =>
    dispatch({
      type: Actions['ADMIN/REQUEST_SAVE_GAME_LISTS']
    }),
  updateListItem: (listName: string, index: number, value: string) =>
    dispatch({
      type: Actions['ADMIN/UPDATE_LIST_ITEM'],
      listName,
      index,
      value
    }),
  appendListItem: (listName: string) =>
    dispatch({
      type: Actions['ADMIN/ADD_LIST_ITEM'],
      payload: listName
    }),
  updateDeletions: (items: IAutoAddingListItem[], listName: string) =>
    dispatch({
      type: Actions['ADMIN/UPDATE_LIST_ITEM_DELETIONS'],
      listName,
      items
    })
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Admin);
