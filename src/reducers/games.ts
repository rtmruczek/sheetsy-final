import { Action } from 'redux';
import { values, head } from 'ramda';
import { IAutoAddingListItem } from 'src/components/AutoAddingList';

export interface IGames {
  games: { [key: string]: IGame };
  markedForDeletion: { [index: string]: IAutoAddingListItem[] };
  isFetching: boolean;
  selectedGame: string;
}

export interface IGame {
  id: string;
  name: string;
  races: any[];
  classes: any[];
  skills: any[];
}

const initialState: IGames = {
  games: {},
  markedForDeletion: {},
  isFetching: false,
  selectedGame: ''
};

export interface UpdateListItemAction extends Action {
  listName: string;
  index: number;
  value: string;
}

export enum Actions {
  // REQUEST_UPDATE_DOCUMENT = 'REQUEST_UPDATE_DOCUMENT',
  // SUCCESS_UPDATE_DOCUMENT = 'SUCCESS_UPDATE_DOCUMENT',
  RECEIVE_PUBLIC_DATA = 'RECEIVE_PUBLIC_DATA',
  REQUEST_PUBLIC_DATA = 'REQUEST_PUBLIC_DATA',

  'ADMIN/REQUEST_SAVE_GAME_LISTS' = 'ADMIN/REQUEST_SAVE_GAME_LISTS',
  'ADMIN/SUCCESS_SAVE_GAME_LISTS' = 'ADMIN/SUCCESS_SAVE_GAME_LISTS',

  'ADMIN/UPDATE_LIST_ITEM' = 'ADMIN/UPDATE_LIST_ITEM',
  'ADMIN/ADD_LIST_ITEM' = 'ADMIN/ADD_LIST_ITEM',

  'ADMIN/UPDATE_LIST_ITEM_DELETIONS' = 'ADMIN/UPDATE_LIST_ITEM_DELETIONS'
}
export const requestData = () => ({
  type: Actions.REQUEST_PUBLIC_DATA
});
export const receiveData = (payload: firebase.firestore.QuerySnapshot) => ({
  type: Actions.RECEIVE_PUBLIC_DATA,
  payload: payload.docs
});

export default (state = initialState, action: any): IGames => {
  switch (action.type) {
    case Actions.RECEIVE_PUBLIC_DATA: {
      const gamesArray = values(action.payload);
      return {
        ...state,
        games: action.payload,
        selectedGame: head(gamesArray).id
      };
    }
    case Actions.REQUEST_PUBLIC_DATA:
    case Actions['ADMIN/REQUEST_SAVE_GAME_LISTS']: {
      return {
        ...state,
        isFetching: true
      };
    }

    case Actions['ADMIN/SUCCESS_SAVE_GAME_LISTS']: {
      return {
        ...state,
        isFetching: true
      };
    }
    case Actions['ADMIN/ADD_LIST_ITEM']: {
      const game = {
        ...state.games[state.selectedGame],
        [action.payload]: state.games[state.selectedGame][
          action.payload
        ].concat([''])
      };
      return {
        ...state,
        games: {
          [state.selectedGame]: game
        }
      };
    }
    case Actions['ADMIN/UPDATE_LIST_ITEM']: {
      const list = [
        ...state.games[state.selectedGame][action.listName].slice(
          0,
          action.index
        ),
        ...[action.value],
        ...state.games[state.selectedGame][action.listName].slice(
          action.index + 1
        )
      ];
      return {
        ...state,
        games: {
          [state.selectedGame]: {
            ...state.games[state.selectedGame],
            [action.listName]: list
          }
        }
      };
    }
    case Actions['ADMIN/UPDATE_LIST_ITEM_DELETIONS']: {
      return {
        ...state,
        markedForDeletion: {
          ...state.markedForDeletion,
          [action.listName]: action.items
        }
      };
    }
    default:
      return state;
  }
};
