import { Action } from 'redux';

import { ICharacter } from '../mappable/character';

export enum Actions {
  AUTH_STATE_CHANGED = 'AUTH_STATE_CHANGED',
  REQUEST_MY_DATA = 'REQUEST_MY_DATA',
  RECEIVE_MY_DATA = 'RECEIVE_MY_DATA'
}

export interface IAuthStateChangedAction extends Action {
  payload: firebase.User;
}

export interface IReceiveMyDataAction extends Action {
  payload: {
    characters: ICharacter[]
  }
}

export const authStateChanged = (
  user: firebase.User
): IAuthStateChangedAction => ({
  type: Actions.AUTH_STATE_CHANGED,
  payload: user
});

export interface IUser {
  id: string | null;
  email: string | null;
  characters: ICharacter[];
}
const initialState: IUser = {
  id: null,
  email: null,
  characters: []
};

export default (state = initialState, action: any) => {
  switch (action.type) {
    case Actions.AUTH_STATE_CHANGED:
      return {
        ...state,
        id: action.payload.uid,
        email: action.payload.email
      };
    case Actions.RECEIVE_MY_DATA:
      return {
        ...state,
        ...action.payload
      }
    default:
      return state;
  }
};
