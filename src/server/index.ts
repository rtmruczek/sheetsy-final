import express = require('express');
import * as dotenv from 'dotenv';
import * as bodyParser from 'body-parser';
import { authMiddleware } from './oauth';

dotenv.config();

const app: express.Application = express();

app.set('port', process.env.PORT || 3100);
app.use(bodyParser.json());

app.get('home');
app.get('*', authMiddleware);

console.info(`app is listening on ${app.get('port')}`);
app.listen(app.get('port'));
