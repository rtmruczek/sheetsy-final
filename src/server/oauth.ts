import { google } from 'googleapis';
import * as express from 'express';

const isDevelopment = process.env.NODE_ENV !== 'production';

const oAuthConfig = {
  clientId: process.env.GOOGLE_API_CLIENT_ID,
  clientSecret: process.env.GOOGLE_API_CLIENT_SECRET,
  redirect: isDevelopment ? 'http://localhost:3000' : process.env.HOSTNAME
};

const scopes = ['https://www.googleapis.com/auth/plus.me'];

export function createConnection() {
  const connection = new google.auth.OAuth2(
    oAuthConfig.clientId,
    oAuthConfig.clientSecret,
    oAuthConfig.redirect
  );

  return connection;
}

export function authMiddleware(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) {
  const auth = createConnection();
  const authUrl = auth.generateAuthUrl({
    scope: scopes
  });

  return res.redirect(authUrl);

  // connection.on('tokens', tokens => {
  //   if (tokens.refresh_token) {
  //   }
  // });
}

export async function getAccountFromAuthToken(code: string) {
  const auth = createConnection();
  const { tokens } = await auth.getToken(code);
  auth.setCredentials(tokens);

  const googlePlusApi = google.plus({ version: 'v1', auth });

  const me = await googlePlusApi.people.get({ userId: 'me' });
  const userGoogleId = me.data.id;
  const userGoogleEmail =
    me.data.emails && me.data.emails.length && me.data.emails[0].value;
  return {
    id: userGoogleId,
    email: userGoogleEmail,
    tokens
  };
}
