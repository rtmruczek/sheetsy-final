import { path, find, dropLast, last } from 'ramda';

export interface ICharacter {
  id: string;
  name: string;
  level: number;
  race: {
    name: string;
  };
  class: {
    name: string;
  }
}


export function getObjectFromPath(
  state: any,
  documentReference: firebase.firestore.DocumentReference
) {
  // console.info(JSON.stringify(state));
  // console.info(`looking up ${documentReference.path}`);
  const pathComponents = ['games'].concat(documentReference.path.split('/'))
  const statePath = dropLast(1, pathComponents)
  const id = last(pathComponents)
  const value = find((object: any) => object.id === id, path(statePath, state) as any[])
  // console.log(value)
  return value
}


export function mapCharacters(characters: firebase.firestore.DocumentData[], state: any): ICharacter[] {
  return characters.map(character => ({
    id: character.id as string,
    name: character.name as string,
    level: character.level as number,
    class: getObjectFromPath(
      state,
      character.class as firebase.firestore.DocumentReference
    ) as { name: string },
    race: getObjectFromPath(
      state,
      character.race as firebase.firestore.DocumentReference
    ) as { name: string }
  }));
}
