// ./types.d.ts
import '@firebase/firestore';
import * as React from 'react';
import { Provider } from 'react-redux';
import createSagaMiddleware from 'redux-saga';
import { createLogger } from 'redux-logger';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import { createStore, applyMiddleware, Store } from 'redux';
import './App.css';
import mySaga from './sagas';
import rootReducer from './reducers';
import Home from './containers/Home';
import Characters from './containers/Characters';
import CharacterDetail from './containers/CharacterDetail';
import Admin from './containers/Admin';
import { Actions } from './reducers/games';

const sagaMiddleware = createSagaMiddleware();
const middleware = [sagaMiddleware, createLogger()];

const store = createStore(
  rootReducer,
  {},
  composeWithDevTools(applyMiddleware(...middleware))
) as Store;
sagaMiddleware.run(mySaga);

store.dispatch({ type: Actions.REQUEST_PUBLIC_DATA });

const routes = (
  <Router>
    <Switch>
      <Route exact={true} path="/" component={Home} />
      <Route exact={true} path="/characters" component={Characters} />
      <Route exact={true} path="/admin" component={Admin} />
      <Route exact={true} path="/characters/:id" component={CharacterDetail} />
    </Switch>
  </Router>
);

class App extends React.Component {
  public render() {
    return <Provider store={store}>{routes}</Provider>;
  }
}

export default App;
